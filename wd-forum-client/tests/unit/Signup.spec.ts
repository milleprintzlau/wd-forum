import { mount } from '@vue/test-utils';
import Signup from '@/views/Signup.vue';

describe('Signup.vue', () => {
  it('renders Signup page', () => {
    const wrapper = mount(Signup);
    const title = wrapper.find('h1');

    expect(title.text()).toBe('Sign Up');
  });

  it('shows error if form is submitted empty', (done) => {
    const wrapper = mount(Signup);
    wrapper.find('button').trigger('click');
    wrapper.vm.$nextTick(() => {
      expect(wrapper.find('#error')).toBeTruthy();
      done();
    });
  });
});
