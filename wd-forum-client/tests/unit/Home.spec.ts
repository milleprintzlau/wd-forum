import { mount } from '@vue/test-utils';
import Home from '@/views/Home.vue';

describe('Home.vue', () => {
  it('renders Home page', () => {
    const wrapper = mount(Home);
    const title = wrapper.find('h1');

    expect(title.text()).toBe('We love web development');
  });
});
