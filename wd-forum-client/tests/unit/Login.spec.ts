import { mount } from '@vue/test-utils';
import Login from '@/views/Login.vue';

describe('Login.vue', () => {
  it('renders Login page', () => {
    const wrapper = mount(Login);
    const title = wrapper.find('h1');

    expect(title.text()).toBe('Login');
  });

  it('shows error if form is submitted empty', (done) => {
    const wrapper = mount(Login);
    wrapper.find('button').trigger('click');
    wrapper.vm.$nextTick(() => {
      expect(wrapper.find('#error')).toBeTruthy();
      done();
    });
  });
});
