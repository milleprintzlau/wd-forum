<?php

use PHPUnit\Framework\TestCase;
require __DIR__ . '/../vendor/autoload.php';
include __DIR__ . '/../items/Topic.php';

final class TopicTest extends TestCase {
	
	private $testDb;
	
	public function setUp(): void {
		parent::setUp();
		$options = [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
		];
		
		try {
			$this->testDb = new PDO('mysql:host=127.0.0.1;port=8889;dbname=WDForum_test', 'test', '', $options);
		} catch (PDOException $ex) {
			print_r($ex->getMessage());
			echo '{"error": true, "errorMessage": "Database connection has failed"}';
		}
	}
	
	
	/** @test */
	public function it_gets_all_topics_correctly() {
		// Arrange
		// Act
		$response = (new Topic())->getAllTopics($this->testDb);

		// Assert
		$this->assertEquals(6, count($response['topics']));
		$this->assertEquals(true, $response['success']);
	}
}
