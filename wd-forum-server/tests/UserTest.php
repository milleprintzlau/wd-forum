<?php

use PHPUnit\Framework\TestCase;
require __DIR__ . '/../vendor/autoload.php';
include __DIR__ . '/../items/User.php';

final class UserTest extends TestCase {
	
	private $testDb;
	
	public function setUp(): void {
		parent::setUp();
		$options = [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
		];
		
		try {
			$this->testDb = new PDO('mysql:host=127.0.0.1;port=8889;dbname=WDForum_test', 'test', '', $options);
		} catch (PDOException $ex) {
			print_r($ex->getMessage());
			echo '{"error": true, "errorMessage": "Database connection has failed"}';
		}
	}
	
	/** @test */
	public function it_creates_new_user() {
		// Arrange
		$q = $this->testDb->prepare('SELECT COUNT(*) as count FROM users');
		$q->execute();
		$initialCount = $q->fetch()->count;

		// Act
		$response = (new User())->createUser($this->testDb, 'A', 'B', 'AB', 'elearning.dbcamp@gmail.com', 'QW123456');
		$q = $this->testDb->prepare('SELECT COUNT(*) as count FROM users');
		$q->execute();
		$finalCount = $q->fetch()->count;
		
		// Assert
		$this->assertEquals(true, $response['success']);
		$this->assertEquals($initialCount + 1, $finalCount);
	}
	
	/** @test */
	public function it_validates_a_user() {
		// Arrange
		$response = (new User())->createUser($this->testDb, 'A', 'B', 'AB', 'elearning.dbcamp@gmail.com', 'QW123456');
		$userId = $response['id'];
		
		// Act
		$response = (new User())->validateUser($this->testDb, $userId);

		$q = $this->testDb->prepare('SELECT verified FROM users WHERE id=:id');
		$q->bindParam(':id', $userId);
		$q->execute();

		$verified = $q->fetch()->verified;
		
		// Assert
		$this->assertEquals("1", $verified);
	}
	
	/** @test */
	public function it_updates_user_to_premium() {
		// Arrange
		$response = (new User())->createUser($this->testDb, 'A', 'B', 'AB', 'elearning.dbcamp@gmail.com', 'QW123456');
		$userId = $response['id'];
		
		// Act
		$response = (new User())->updateUserToPremium($this->testDb, $userId);
		
		$q = $this->testDb->prepare('SELECT premium FROM users WHERE id=:id');
		$q->bindParam(':id', $userId);
		$q->execute();
		
		$premium = $q->fetch()->premium;
		
		// Assert
		$this->assertEquals("1", $premium);
	}
	
	/** @test */
	public function it_authenticates_a_user() {
		// Arrange
		$newUserResponse = (new User())->createUser($this->testDb, 'A', 'B', 'AB', 'elearning.dbcamp@gmail.com', 'QW123456');
		$userId = $newUserResponse['id'];

		(new User())->validateUser($this->testDb, $userId);
		
		// Act
		$response = (new User())->authenticateUser($this->testDb, 'AB', 'QW123456');
		
		// Assert
		$this->assertEquals(true, $response['success']);
	}
	
	public function tearDown(): void {
		parent::tearDown(); // TODO: Change the autogenerated stub
		
		$q = $this->testDb->prepare('DELETE FROM users');
		$q->execute();
	}
}
