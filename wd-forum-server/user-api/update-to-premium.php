<?php

require '../initialize.php';
require '../validate-session.php';
include '../items/User.php';

$userId = $_POST['userId'];
$token = $_POST['token'];
$cardNumber = $_POST['cardNumber'];
$cardName = $_POST['cardName'];
$month = $_POST['month'];
$year = $_POST['year'];
$cvvNumber = $_POST['cvvNumber'];

if ($userId !== $_SESSION['userId']) {
	echo json_encode(['error' => true, 'errorMessage' => 'You do not have permission for this action!']);
	exit();
}

if ($token !== $_SESSION['csrf_token']) {
	echo json_encode(['error' => true, 'errorMessage' => 'Permission denied!']);
	exit();
}

if (!$userId) {
	echo json_encode(['error' => true, 'errorMessage' => 'Missing user id!']);
	exit();
}

if (strlen($cardNumber) !== 16) {
	echo json_encode(['error' => true, 'errorMessage' => 'Invalid card number!']);
	exit();
}

if (!is_numeric($cardNumber) || !is_numeric($month) || !is_numeric($year) || !is_numeric($cvvNumber)) {
	echo json_encode(['error' => true, 'errorMessage' => 'Invalid data!']);
	exit();
}

$response = (new User())->encryptAndStoreCardInfo($CRUDDBConnection, $userId, $cardNumber, $cardName, $month, $year, $cvvNumber);
if ($response['error']) {
	echo json_encode(['error' => true, 'errorMessage' => $response['errorMessage']]);
	exit();
}

$userResponse = (new User())->updateUserToPremium($CRUDDBConnection, $userId);
echo json_encode($userResponse);
