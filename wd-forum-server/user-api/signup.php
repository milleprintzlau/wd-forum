<?php

require '../initialize.php';
include '../items/User.php';

$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$username = $_POST['username'];
$email = $_POST['email'];
$password = $_POST['password'];

if (!$firstName || !$lastName || !$username || !$email || !$password) {
	echo json_encode(['error' => true, 'errorMessage' => 'All the fields needs to be fulfilled!']);
	exit();
}

if (!preg_match('/@/', $email)) {
	echo json_encode(['error' => true, 'errorMessage' => 'Please enter a valid email address!']);
	exit();
}

if (strlen($password) < 8) {
	echo json_encode(['error' => true, 'errorMessage' => 'Password needs to be at least 8 characters!']);
	exit();
}

if (!preg_match('/[A-Z]/', $password)) {
	echo json_encode(['error' => true, 'errorMessage' => 'Password needs to contain at least a capital letter!']);
	exit();
}

if (!preg_match('/[0-9]/', $password)) {
	echo json_encode(['error' => true, 'errorMessage' => 'Password needs to contain at least one digit!']);
	exit();
}

$response = (new User())->createUser($CRUDDBConnection, $firstName, $lastName, $username, $email, $password);

echo json_encode($response);
