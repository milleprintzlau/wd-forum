<?php

require '../initialize.php';
require '../validate-session.php';
include '../items/Topic.php';
include '../items/User.php';

$response = (new User())->isUserPremium($readOnlyDBConnection, $_SESSION['userId']);

if ($response['success'] && !$response['isPremium']) {
	echo json_encode(['error' => true, 'errorMessage' => 'You do not have permission for this action!']);
	exit();
}

$response = (new Topic())->getAllTopics($readOnlyDBConnection);

echo json_encode($response);
