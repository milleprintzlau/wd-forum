<?php

require '../initialize.php';
require '../validate-session.php';
include '../items/Quiz.php';
include '../items/User.php';

$userId = $_POST['userId'];
$quizId = $_POST['quizId'];
$quizScore = $_POST['quizScore'];

if ($userId !== $_SESSION['userId']) {
	echo json_encode(['error' => true, 'errorMessage' => 'You do not have permission for this action!']);
	exit();
}

$response = (new User())->isUserPremium($CRUDDBConnection, $_SESSION['userId']);

if ($response['success'] && !$response['isPremium']) {
	echo json_encode(['error' => true, 'errorMessage' => 'You do not have permission for this action!']);
	exit();
}

$response = (new Quiz())->saveAnsweredQuiz($CRUDDBConnection, $quizId, $quizScore, $userId);

echo json_encode($response);
