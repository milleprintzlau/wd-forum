<?php

require '../initialize.php';
require '../validate-session.php';
include '../items/User.php';
include '../items/Quiz.php';

$quizId = $_POST['quizId'];

$response = (new User())->isUserPremium($CRUDDBConnection, $_SESSION['userId']);

if ($response['success'] && !$response['isPremium']) {
	echo json_encode(['error' => true, 'errorMessage' => 'You do not have permission for this action!']);
	exit();
}

$response = (new Quiz())->getQuiz($CRUDDBConnection, $quizId);

echo json_encode($response);
