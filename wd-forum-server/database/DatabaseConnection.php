<?php

final class DatabaseConnection {

  private $db;

	/**
	 * DatabaseConnection constructor.
	 * @param string|null $accessType
	 */
	public function __construct(string $accessType = 'read_only') {
		$connection = 'mysql:host=' . $_ENV['DB_HOST'] . '; dbname=' . $_ENV['DB_NAME'] . '; charset=utf8mb4';
		$options = [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
		];
		
		$dbUserName = $_ENV['DB_READ_ONLY_USERNAME'];
		$dbPassword = $_ENV['DB_READ_ONLY_PASSWORD'];
		
		if ($accessType === 'CRUD_permissions') {
			$dbUserName = $_ENV['DB_CRUD_PERMISSIONS_USERNAME'];
			$dbPassword = $_ENV['DB_CRUD_PERMISSIONS_PASSWORD'];
		}

		try {
			$this->db = new PDO($connection, $dbUserName, $dbPassword, $options);
		} catch (PDOException $ex) {
			echo '{"error": true, "errorMessage": "Database connection has failed"}';
		}
	}
	
	public function getConnection(): PDO {
		return $this->db;
	}
}

