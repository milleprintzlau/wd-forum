<?php

final class Comment {
	public function createComment($db, $text, $userId, $postId, $encodedFileContents = null): array {
		try {
			$q = $db->prepare('INSERT INTO comments VALUES(null, :text, :file_contents, :user_id, :post_id)');
			$q->bindValue(':text', $text);
			$q->bindValue(':file_contents', $encodedFileContents);
			$q->bindValue(':user_id', $userId);
			$q->bindValue(':post_id', $postId);
			$q->execute();
			
			return ['success' => true, 'id' => $db->lastInsertId()];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Creating a new comment has failed!'];
		}
	}
	
	public function getCommentsForPost($db, $postId): array {
		try {
			$q = $db->prepare('SELECT * FROM comments WHERE post_id = :post_id');
			$q->bindValue(':post_id', $postId);
			$q->execute();
			
			return ['success' => true, 'comments' => $q->fetchAll()];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not fetch comments for post!'];
		}
	}
}
