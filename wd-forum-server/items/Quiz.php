<?php

include '../items/Diploma.php';

final class Quiz {

	public function getQuiz($db, $quizId): array {
		try {
			$q = $db->prepare('SELECT * FROM questions INNER JOIN quizzes ON quizzes.id = questions.quiz_id WHERE quizzes.id = :id;');
			$q->bindValue(':id', $quizId);
			$q->execute();

			$data = $q->fetchAll();
			$quiz = new stdClass();
			$quiz->id = $quizId;
			$quiz->title = $data[0]->title;
			$quiz->questions = $data;

			return ['success' => true, 'quiz' => $quiz];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not fetch quiz!'];
		}
	}
	
	public function saveAnsweredQuiz($db, $quizId, $quizScore, $userId): array {
		try {
			$q = $db->prepare('INSERT INTO answeredQuizzes VALUES(null, :quiz_id, :quiz_score, :user_id)');
			$q->bindValue(':quiz_id', $quizId);
			$q->bindValue(':quiz_score', $quizScore);
			$q->bindValue(':user_id', $userId);
			$q->execute();
			
			$answeredQuizId = $db->lastInsertId();
			
			return (new Diploma())->createDiploma($db, $answeredQuizId);
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not save quiz!'];
		}
	}
	
	public function getCountOfAnsweredQuizzes($db, $userId): array {
		try {
			$q = $db->prepare('SELECT COUNT(*) AS count FROM answeredQuizzes WHERE user_id = :userId');
			$q->bindValue(':userId', $userId);
			$q->execute();
			
			return ['success' => true, 'count' => $q->fetchColumn()];
			
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not fetch count!'];
		}
	}
}
