<?php

require '../initialize.php';
include '../items/Comment.php';

$postId = $_POST['postId'];

$response = (new Comment())->getCommentsForPost($CRUDDBConnection, $postId);

echo json_encode($response);
