<?php

require '../initialize.php';
include '../items/Post.php';

$postId = $_POST['postId'];

$response = (new Post())->getPost($CRUDDBConnection, $postId);

echo json_encode($response);
