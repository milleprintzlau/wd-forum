<?php

require '../initialize.php';

session_start();
$sessionId = $_POST['sessionId'];

if ($sessionId !== session_id()) {
	echo json_encode(['error' => true, 'errorMessage' => 'You do not have permission for this action!']);
	exit();
}

$token = base64_encode(random_bytes(64));
$_SESSION['csrf_token'] = $token;

echo json_encode(['success' => true, 'token' => $token]);
