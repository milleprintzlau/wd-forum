---
layout: default
title: Client
---

## Setup
1. ### Install npm
```npm
npm install
```
2. ### Run this command to start the live server
```npm
npm run serve
```
3. ### Run this command to compile and minify files for production
```npm
npm run build
```
4. ### In order to do linting and fix files, run this command
```npm
npm run lint
```
### Customize configuration
See <a href="https://cli.vuejs.org/config/#global-cli-config">Configuration Reference</a>

### Table of contents
* <a href="{{site.baseurl}}/documentation/views/">Views</a>
* <a href="{{site.baseurl}}/documentation/routing/">Routing</a>
* <a href="{{site.baseurl}}/documentation/components/">Components</a>
* <a href="{{site.baseurl}}/documentation/assets/">Assets</a>
* <a href="{{site.baseurl}}/documentation/test/">Test</a>