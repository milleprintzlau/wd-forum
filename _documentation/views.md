---
layout: default
title: Views
---

The views folder contains the web pages of the application. Find the views folder inside the src folder.


We have the following views:
* AddNewPost.vue
* Courses.vue
* Forum.vue
* ForumPost.vue
* Home.vue
* Login.vue
* MyAccount.vue
* Quiz.vue
* Signup.vue
* Inside the topics folder you find the views for each course

The views are named in a way that describes which page it holds. 

## How to create a view

### A view must contain:
* a template tag that wraps around all content.
* a <div> tag inside the template tag wrapping around the rest of the content.
* Scripts that are written under the template. We write TypeScript.
* Styles for the specific view are written under the script inside a <style> tag.

The views get rendered inside the App.vue that holds the navigation, footer and some more styling.

Create a file inside the views folder. It should be named logically and it must start with a capital letter.
_Example: Login.vue_

See this example of a view:
```text
<template>
  <div class="forum page">
    <div class="intro-container">
      <div class="intro-text">
        <h1>Forum</h1>
      </div>
    </div>

    <div v-if="posts">
      <ForumPostContainer v-for="post in posts" :key="post.id" :post="post" :sessionId="sessionId" :userId="userId" />
    </div>

    <QuestionBanner />
  </div>
</template>

<style lang="scss">

</style>

<script lang="ts">
import {
  defineComponent, ref,
} from 'vue';
import QuestionBanner from '@/components/layout/QuestionBanner.vue';
import ForumPostContainer from '@/components/forum/ForumPostContainer.vue';

export default defineComponent({
  name: 'Forum',
  props: {
    sessionId: String,
    userId: String,
  },
  components: {
    QuestionBanner,
    ForumPostContainer,
  },
  setup() {
    const posts = ref<any>(undefined);

    fetch(`${process.env.VUE_APP_API_URL}/forum-api/get-posts.php`, {
      method: 'POST',
      credentials: 'include',
    }).then((response) => response.json())
      .then((content) => {
        if (content.success) {
          posts.value = content.posts;
        }
      });

    return {
      posts,
    };
  },
});
</script>
```
