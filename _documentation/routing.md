---
layout: default
title: Routing
---

The router folder holds an index.ts with all the URL routes. The way we do routing is by dynamically rendering a page-level component.

## How to add a URL route 
1. Go to the index.ts file inside the router folder that you find inside the src folder
2. Import the view component.
_Example:_ 
```ts
import Login from '../views/Login.vue';
```
3. Add the path, the name and the view component.
_Example:_ 
```ts
const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
```

## More information
Get more information about the Vue Router on their <a href="https://router.vuejs.org/">Website</a>.