---
layout: default
title: Test
---

Our Vue application are provided with testing tools for Unit testing and end-to-end testing.
This is how you run the tests in order to build new features, refactor code and fix bugs.

### Run your unit tests
```npm
npm run test:unit
```
### Run your end-to-end tests
```npm
npm run test:e2e
```